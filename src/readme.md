# Exerc�cios L�gica

## B�sicos

- Opera��es
- Tipos de vari�vel
- Leitura e impressão no console
- Condicionais
- La�os
- Vetores
- Convers�o de tipo

## Intermedi�rios

- Recurs�o
- Utils (Random, ArrayList, HashMap)
- Valida��o de dados
- Tratamento de erros
- Fun��es
- Leitura/Grava��o de arquivo

## Avan�ados

- Fatora��o de c�digo em classes
- Ca�a Niquel
- Jogo 21
 
