package cacaNiquel;

public class Visor {
	
	public void imprimir(int sorteios[], int pontos) {
		for(int i = 0; i < sorteios.length; i++) {
			int sorteio = sorteios[i];
			String valor = Rolo.getValor(sorteio);
			
			System.out.print(String.format(" %s |", valor));
		}
		System.out.println("");
		System.out.println(String.format("Sua pontua��o � %d.", pontos));
	}
}
