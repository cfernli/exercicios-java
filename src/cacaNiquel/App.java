package cacaNiquel;

/**
 * Crie um programa que simula o funcionamento de um ca�a n�quel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte crit�rio:
 * 
 * Caso hajam tr�s valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam tr�s valores iguais: 1000 pontos
 * Caso hajam tr�s valores "7": 5000 pontos
 * 
 * {"abacaxi", "framboesa", "banana", "pera", "7"}
 */
public class App {
	public static void main(String[] args) {
		Motor motor = new Motor();
		Visor visor = new Visor();
		Calculador calculador = new Calculador();
		
		int[] sorteios = new int[3];
		
		for(int i = 0; i < 3; i++) {
			sorteios[i] = motor.sortear();
		}
		
		int pontos = calculador.calcular(sorteios);
		
		visor.imprimir(sorteios, pontos);
	}
}
