package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Crie um programa que l� os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior a� 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * Jo�o da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails {
	public static void main(String[] args) {
		
//		ArrayList<String> lista = new ArrayList<> ();
//		lista.add("Uma palavra");
//		lista.get(0);
//		lista.size();
//		lista.remove(0);
		
		StringBuilder emails = new StringBuilder();
		
		Path outputPath = Paths.get("emails.txt");
		List<String> lista = lerContas();
		
		if(lista == null) {
			System.out.println("N�o foi poss�vel ler o arquivo de contas.");
			return;
		}
		
		try {
//			List<String> lista = Files.readAllLines(path);
//			for (int i= 0; i < lista.size(); i++) {
//				System.out.println(lista.get(i));
//				String[] partes = lista.split("'");
//			}
// outras opcoes de iteracao na lista			
			for(String linha: lista) {
//				System.out.println(linha);
				String[] partes = linha.split(",");
//				System.out.println(partes[4]);
				
				int saldo = Integer.parseInt(partes[4]);
				
				if (saldo > 7000) {
					String email = String.format("%s %s<%s>, ", partes[1], partes[2], partes[3]); 
					emails.append(email);
//					email.append(partes[1] + " " + partes[2] "<" + partes[3] + ">,");
//					System.out.println(saldo);
				}
			}
//			lista.forEach(linha -> System.out.println(linha));
			System.out.println(emails.toString());
//		}
	        Files.write(outputPath, emails.toString().getBytes());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    public static List<String> lerContas(){
		
		Path path = Paths.get("src/contas.csv");
		List<String> lista;
		
		try {
			lista = Files.readAllLines(path);
			lista.remove(0);
			return lista;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}
}



