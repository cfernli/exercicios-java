package b_intermediarios;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * Crie um programa que recebe um todo list e a escreve em um arquivo txt.
 * 
 * A cada itera��o, o sistema deve pedir uma  �nica tarefa e exibir a possibilidade
 * de encerrar o programa.
 * 
 * Quando o usu�rio encerrar o programa, deve-se gerar o arquivo txt com as tarefas
 * que foram inseridas.
 *
 */
public class ListaTarefas2 {
	public static void main(String[] args) {
		Path path = Paths.get("teste.txt");
		boolean saida = false;
		
		Scanner scanner = new Scanner (System.in);
		String palavra = "";
//		StringBuilder palavar = new StringBuilder;

		while (!saida)
		{
			
			System.out.println("Digite uma palavra (sair para finalizar):");
            String texto = scanner.nextLine();
            palavra += texto .concat("|");
//            palavra.append(texto + "\n");
            
			if ("sair".equals(texto))
				saida = true;
		}
				
		try {
			Files.write(path,  palavra.getBytes());
				
		} catch (Exception e) {
//			e.printStackTrace();
			System.out.println("Erro ao gravar o arquivo!");
		}
		scanner.close();
		
	}
}

