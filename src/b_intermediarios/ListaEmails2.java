package b_intermediarios;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Crie um programa que l� os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior a� 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * Jo�o da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 */
public class ListaEmails2 {
	public static void main(String[] args) {
		List<String> lista = lerContas();
		
		if(lista == null) {
			System.out.println("N�o foi poss�vel ler o arquivo.");
			return;
		}

		StringBuilder emails = new StringBuilder();
		
		for(String linha: lista) {
			String[] partes = linha.split(",");
			
			int saldo = Integer.parseInt(partes[4]);
			
			if(saldo > 7000) {
				String email = String.format("%s %s<%s>, ", partes[1], partes[2], partes[3]);
				emails.append(email);
			}
		}
		
		gravarArquivo(emails);
		System.out.println("Processamento conclu�do com sucesso.");
	
	}
	
	public static List<String> lerContas(){
		Path path = Paths.get("src/contas.csv");
		
		List<String> lista;

		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}
		
		lista.remove(0);
		
		return lista;
	}
	
	public static void gravarArquivo(StringBuilder emails) {
		Path outputPath = Paths.get("emails.txt");
		
		try {
			Files.write(outputPath, emails.toString().getBytes());
		} catch (IOException e) {
			System.out.println("N�o foi poss�vel gravar o arquivo.");
		}
	}
}
