package b_intermediarios;

import java.util.Random;

/**
 * Crie um programa que simula o funcionamento de um ca�a n�quel, da seguinte forma:
 * 
 * 1- Sorteie 3x um valor dentre as strings do vetor de valores
 * 2- Compare os valores sorteados e determine os pontos obtidos usando o
 * seguinte crit�rio:
 * 
 * Caso hajam tr�s valores diferentes: 0 pontos
 * Caso hajam dois valores iguais: 100 pontos
 * Caso hajam tr�s valores iguais: 1000 pontos
 * Caso hajam tr�s valores "7": 5000 pontos
 * 
 */
public class CacaNiquel {
	public static void main(String args[]) {
		String[] valores = {"abacaxi", "framboesa", "banana", "pera", "7"};
		
		
		Random gerador = new Random();
//		int val = gerador.nextInt(5);
		String val1 = valores[gerador.nextInt(5)];
		String val2 = valores[gerador.nextInt(5)];
		String val3 = valores[gerador.nextInt(5)];
		
		System.out.println("Voc� escolheu: ");
		System.out.println(val1 + ", " + val2 + " e " + val3 + "\n");
		
		
		if ((val1 == "7") && (val2 == "7") && (val3 == "7")){
			System.out.println("5000 pontos");
		} else {
		    if (val1.equals(val2) && val2.equals(val3)){
			    System.out.println("1000 pontos");
		    } else {
			   if ((val1.equals(val2)) || (val2.equals(val3)) || (val3.equals(val1))){
				   System.out.println("100 pontos");
			   } else {
				   System.out.println("0 pontos");
			   }
				  
		    }
	      }
    }
}

