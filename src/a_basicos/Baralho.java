package a_basicos;

/**
 * Crie um programa que imprima todas as cartas do baralho.
 * 
 * Exemplo: 
 * 
 * �s de Ouros
 * �s de Espadas
 * �s de Copas
 * �s de Paus
 * Dois de Ouros
 * ...
 * 
 */
public class Baralho {
	public static void main(String[] args) {
		String[] ranks = {"�s", "Dois", "Tr�s", "Quatro"};
		String[] naipes = {"Paus", "Ouros", "Copas", "Espadas"};
		
		for(int i = 0; i < ranks.length; i++) {
			for(int j = 0 ; j < naipes.length; j++) {
				System.out.println(ranks[i] + " de " + naipes[j] );
			}
		}
	}
}
