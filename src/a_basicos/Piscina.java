package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe as tr�ss dimenss�es de uma piscina
 * (largura, comprimento, profundidade) e retorne a sua capacidade
 * em litros.
 */
public class Piscina {
	private static Scanner scanner;

	public static void main(String[] args) {
		
		scanner = new Scanner (System.in);
		
		System.out.println("Digite a largura: ");
		double largura = scanner.nextInt();
		
		System.out.println("Digite o Comprimento: ");
		double comprimento = scanner.nextInt();
		
		System.out.println("Digite a profundidade: ");
		double profundidade = scanner.nextInt();
		
		double volume = (largura * comprimento * profundidade) * 1000;
		
		System.out.println("O volume da piscina �: " + volume + " litros");
		
		scanner.close();
	}
}

