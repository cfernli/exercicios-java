package a_basicos;

import java.util.Scanner;

/**
 * Crie um programa que recebe uma palavra do terminal e determina
 * se ela � um pal�ndromo.
 * 
 * Exs: 
 * 
 * input: ovo
 * output: A palavra ovo � um pal�ndromo
 * 
 * input: jose
 * output: A palavra jose n�o � um pal�ndromo 
 */
public class Palindromo {
	public static void main(String args[]) {
		
		Scanner scanner = new Scanner (System.in);
		
		System.out.println("Digite uma palavra: ");
		String palavra = scanner.next();
		String palavra2 = "";
		
		int i = palavra.length()-1;
		while (i>=0)
		{
			char letra =  palavra.charAt(i);
			palavra2 += letra;
			i--;
		}
		
		if (!palavra.equals(palavra2)) 		
			System.out.println("Esta palavra n�o � um polindromo");		
		else 		
			System.out.println("Esta palavra � um polindromo");
				
		scanner.close();
		
	}
}



