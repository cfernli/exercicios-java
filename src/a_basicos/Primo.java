package a_basicos;

/**
 * Crie um programa que imprima todos os n�meros
 * primos de 1 � 100
 */
public class Primo {
	public static void main(String[] args) {
		System.out.print(1 + " ");
		for(int i = 2; i <= 100; i++) {
			boolean ePrimo = true;
			for(int j = 2; j < i; j++) {
				if(i % j == 0) {
					ePrimo = false;
				}
			}
			if(ePrimo) {
				System.out.print(i + " ");
			}
		}
	}
}
