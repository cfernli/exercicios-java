package c_avancados;

/**
 * O problema de Josephus:
 * Josephus foi convidado para participar de um programa de audit�rio. Nesse
 * programa, os participantes convidados devem formar um c�rculo. O apresentador
 * remove os participantes do c�rculo de forma alternada: o primeiro fica no
 * jogo, o segundo sai, o terceiro fica, o quarto sai, e assim sucessivamente.
 *
 * Como Josephus tem um racioc�nio muito r�pido, ele contou a quantidade de
 * convidados e se posicionou no c�rculo na posi��o correta, e venceu o jogo,
 * ganhando o grande pr�mio do programa!
 *
 * Escreva um programa que resolva o 'problema de Josephus'. Dado um n�mero N
 * de participantes ordenados em c�rculo, determine qual a posi��o em que algu�m
 * deve ficar para vencer o jogo.
 *
 * O n�mero N de participantes � lido da entrada padr�o e a posi��o correta do
 * vencedor � impressa na sa�da padr�o.
 *
 */
public class Josephus {
	public static void main(String[] args) {

	}
}

