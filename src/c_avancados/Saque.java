package c_avancados;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

/**
 * Crie um programa que fa�a um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * n�o seja encontrado ou caso o valor n�o esteja dispon�vel.
 * 
 * Ap�s o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner (System.in);
		List<String> lista1 = lerContas();
		StringBuilder listaSaida = new StringBuilder();
		
		System.out.println("Digite um ID:");
		int idCliente = scanner.nextInt();
        
        System.out.println("Digite um valor:");
        int valorSaque = scanner.nextInt();
        
        for(String linha: lista1) {

			String[] partes = linha.split(",");
			
			int idClienteLista = Integer.parseInt(partes[0]);	
			int valorSaldoLista = Integer.parseInt(partes[4]);
			
			if (idClienteLista == idCliente) {
				int saldoAtual = retornaSaldo(valorSaldoLista, valorSaque);
				String arqSaida = String.format("%s, %s, %s, %s, %s; ", partes[0], partes[1], partes[2], partes[3], saldoAtual); 
				listaSaida.append(arqSaida);
			//	gravarContas(listaSaida);

			} else {
				String arqSaida = linha + "; "; 
				listaSaida.append(arqSaida);
			//	gravarContas(listaSaida);
			}
			
		}	
        gravarContas(listaSaida);
	}
	
	public static List<String> lerContas(){
		
		Path path = Paths.get("src/contas.csv");
		
		List<String> lista;
		
		try {
			lista = Files.readAllLines(path);
			lista.remove(0);
			return lista;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}
	
	public static void gravarContas(StringBuilder lista){
		
		Path outputPath = Paths.get("contas.txt");
		
		try {
			Files.write(outputPath, lista.toString().getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	public static int retornaSaldo(int saldo, int saque) {
		int saldoAtual = 0;
		if (saque > saldo) {
			System.out.println("Saldo insuficiente.");
			saldoAtual = saldo;
		}else {
		    saldoAtual = saldo - saque;
		    System.out.println("Saldo atualizado.");
	    }
		return saldoAtual;		
	}
}
