package c_avancados;

/**
 * Crie um programa que realiza o sorteio de 20 n�meros
 * de 1 � 50, de forma que um n�mero sorteado n�o pode ser repetido.
 * 
 * Ap�s o sorteio, o progama deve conferir uma cartela pr�-determinada
 * e determinar o resultado dentre as poss�veis op��es:
 * 
 * - Bingo: cartela cheia
 * - Linha: linha cheia (vertical ou horizontal)
 * - Erro: nenhuma das anteriores
 *
 * O programa deve imprimir os n�meros sorteados e o resultado.	
 */
public class Bingo {

	public static void main(String args[]) {
		int[][] cartela = {
			{10, 21, 34, 43},
			{7, 15, 11, 50},
			{41, 9, 33, 2},
			{1, 2, 34, 49}
		};
	}
}

