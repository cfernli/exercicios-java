package jogo21;

import java.util.Scanner;

public class Jogo {
	
	public static void main (String args[]) {
		
		Scanner scanner = new Scanner(System.in);
		
		int topo = 52;
		int jogador = 0;
		Baralho baralho = new Baralho();
		boolean continua = true;
		int qJogadores = 0;
		int maisCarta = 0;
		Carta cartaSorteada = null;
		
		System.out.println ("J O G O   2 1");
		System.out.println ("Digite a quantidade de jogadores\n");
		qJogadores = scanner.nextInt();
		Jogador jogadores[] = new Jogador[qJogadores];
		criaJogadores(jogadores, qJogadores);

//		System.out.println (cartas[topo].valor + " " + cartas[topo].naipe);
	
		while (continua) {
			System.out.println ("Vez do jogador " + jogador + ".\n");
			System.out.println ("Deseja mais uma cata? (S/N)");
			maisCarta = scanner.nextInt();
	
			if (maisCarta == 1) {
				cartaSorteada = baralho.proximaCarta(topo);
				topo--;
				System.out.println ("Sua carta: " + cartaSorteada.valor + " " + cartaSorteada.naipe);
				jogadores[jogador].pontuacao = jogadores[jogador].pontuacao + cartaSorteada.valorReal;
				System.out.println ("Pontuacao: " + jogadores[jogador].pontuacao);
			
				if (jogadores[jogador].pontuacao > 21) {
					System.out.println ("Voce perdeu!\n");
					System.out.println ("Pontuacao: " + jogadores[jogador].pontuacao);
					jogadores[jogador].ativo = false;
					jogador++;
				} else {
				      if (jogadores[jogador].pontuacao == 21) {
					      System.out.println ("Voce ganhou!\n");
					      System.out.println ("Pontuacao: " + jogadores[jogador].pontuacao);
					      jogadores[jogador].ativo = false;
					      jogador++;
				      }
				  }
			} else {
				System.out.println ("Fim!");
				jogadores[jogador].ativo = false;
				jogador++;
			}
			continua = verificaContinuacaoJogo(jogadores, qJogadores);      
		}
	}
				
	public static boolean verificaContinuacaoJogo(Jogador[] jogadores, int qJogadores) {
		for (int i=0; i<qJogadores; i++) {
            if (jogadores[i].ativo) {return true;} 
		}
        return false;
	}
	
    public static void criaJogadores(Jogador[] jogadores, int n) {
    	for (int i=0; i<n; i++) {
    		jogadores[i] = new Jogador(); 
    	}
	}
	
}

