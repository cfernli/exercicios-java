package jogo21;

public class Carta {
	
	private static String[] naipes = {"vazio", "Copas", "Paus", "Ouros", "Espadas"};
	String valor;
	String naipe;
	int valorReal;
	
	public Carta (int v, int n) {
		
		this.valorReal = converteValorReal(v);
		this.valor = converteValor(v);
		this.naipe = converteNaipe(n);

	}
	
	private static String converteNaipe (int n) {
		
		return naipes[n];
	}
	
	private static String converteValor (int v) {
		String vAux = "";
		if (v == 1) {vAux = "As";} 
		else {
			if (v == 11) {vAux = "Valete";}
			else {
				if (v == 12) {vAux = "Dama";}
				else {
				    if (v == 13) {vAux = "Rei";}
				    else {
				    	vAux = Integer.toString(v);
				    }
				}
			}
		}
		return vAux;
	}
	
	private static int converteValorReal (int v) {
		int vAux = 0;
		if (v == 1) {vAux = 1;} 
		else {
			if (v == 11) {vAux = 10;}
			else {
				if (v == 12) {vAux = 10;}
				else {
				    if (v == 13) {vAux = 10;}
				    else {
				    	vAux = v;
				    }
				}
			}
		}
		return vAux;
	}
}

