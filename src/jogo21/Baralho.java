package jogo21;

import java.util.Random;
//import java.util.ArrayList;
//import java.util.List;

public class Baralho {
	
//	List<Carta> cartas = new ArrayList<Carta>();
	private Carta cartas[] = new Carta[52];
	
	public Baralho() {
		int topo = 0;
		for (int n=1; n<5; n++) {
			for (int v=1; v<14; v++){	
				cartas[topo] = new Carta(v,n);
//				this.cartas.add(new Carta(v, n));
				System.out.println (cartas[topo].valor + " " + cartas[topo].naipe);
				topo++;
			}
		}
		embaralhar();
	}
	
	public void embaralhar() {
		Random random = new Random();
		for (int i = 0; i<51; i++){
			int r = random.nextInt(52);
			Carta aux = null;
			aux = cartas[r];
			cartas[r] = cartas[i];
			cartas[i] = aux;
		}
	}
	
	public Carta proximaCarta(int topo) {
		Carta aux = null;
		if (topo > 0) {
			topo--;
			aux = cartas[topo];
		}
		return aux;
	}

}

